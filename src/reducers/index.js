/**
 * State of the app and reducer
 */
import { steps, pages, types, langs } from '../constants';

const initialPage = steps.LANGUAGE;

export const initialState = {
  page: initialPage,
  step: pages.indexOf(initialPage),
  lang: langs[Object.keys(langs)[0]],
  popupDisclaimer: false,
  scrolled: false,
  inputName: '',
  inputEmail: '',
  checkboxSubscription: true,
  checkboxTerms: false,
  drink: undefined,
  machine: undefined,
  moment: undefined,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.STEP_NEXT: {
      const step = state.step < pages.length ? state.step + 1 : state.step;

      return {
        ...state,
        page: pages[step],
        step,
      };
    }

    case types.STEP_PREVIOUS: {
      const step = state.step > 0 ? state.step - 1 : state.step;

      return {
        ...state,
        page: pages[step],
        step,
      };
    }

    case types.RESTART:
      return {
        ...state,
        page: initialPage,
        step: pages.indexOf(initialPage),
        inputName: '',
        inputEmail: '',
        checkboxSubscription: true,
        checkboxTerms: false,
        drink: undefined,
        machine: undefined,
        moment: undefined,
      };

    case types.LANGUAGE_SELECT:
      return {
        ...state,
        lang: langs[action.lang],
      };

    case types.INPUT_CHANGE:
      return {
        ...state,
        [action.input]: action.value,
      };

    case types.CHECKBOX_TOGGLE:
      return {
        ...state,
        [action.checkbox]: !state[action.checkbox],
      };

    case types.ITEM_SELECT:
      return {
        ...state,
        [action.item]: action.value,
      };

    case types.ITEM_DESELECT:
      return {
        ...state,
        [action.item]: undefined,
      };

    case types.POPUP_DISCLAIMER_TOGGLE:
      return {
        ...state,
        popupDisclaimer: !state.popupDisclaimer,
      };

    case types.SCROLL_TOGGLE:
      return {
        ...state,
        scrolled: !state.scrolled,
      };

    case types.DATA_SUBMIT:
      return {
        ...state,
        inputName: '',
        inputEmail: '',
        checkboxSubscription: true,
        checkboxTerms: false,
        drink: undefined,
        machine: undefined,
        moment: undefined,
      };

    default:
      return state;
  }
};

