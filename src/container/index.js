/**
 * App Container
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { steps, langs, drinks } from '../constants';
import * as Components from '../components';
import './styles.css';

const Page = (props) => {
  switch (props.page) {
    case steps.PERSONAL:
      return (
        <Components.PagePersonal
          {...props}
        />);

    case steps.DRINKS:
      return (
        <Components.PageDrinks
          {...props}
          drinks={drinks}
          selectItem={index => props.selectItem('drink', index)}
          deselectItem={() => props.deselectItem('drink')}
        />);

    case steps.MACHINES:
      return (
        <Components.PageMachines
          lang={props.lang}
          selected={props.machine}
          selectItem={index => props.selectItem('machine', index)}
          nextStep={props.nextStep}
          deselectItem={() => props.deselectItem('machine')}
        />);

    case steps.MOMENTS:
      return (
        <Components.PageMoments
          lang={props.lang}
          selected={props.moment}
          selectItem={index => props.selectItem('moment', index)}
          nextStep={() => {
            props.nextStep();
            props.submitData();
          }}
          deselectItem={() => props.deselectItem('moment')}
        />);

    case steps.MERCI:
      return (
        <Components.PageMerci
          lang={props.lang}
          onRestart={props.restart}
        />);

    case steps.LANGUAGE:
    default:
      return <div className="app-page" />;
  }
};

const Navigation = (props) => {
  switch (props.page) {
    case steps.LANGUAGE:
      return (
        <Components.NavigationLanguage
          langs={Object.keys(langs)}
          onSelect={props.selectLanguage}
        />
      );

    case steps.MERCI:
      return (
        <Components.NavigationMerci
          label={props.lang.merci}
          onRestart={props.restart}
        />
      );

    default:
      return (
        <Components.Navigation
          step={props.step}
          name={props.lang.steps[props.step]}
          backLabel={props.lang.restart}
          onBack={props.previousStep}
          onRestart={props.restart}
        />
      );
  }
};

const App = props => (
  <div className="app">
    {props.popupDisclaimer &&
      <Components.PopupDisclaimer
        langName={props.lang.langName}
        togglePopup={props.togglePopupDisclaimer}
      />
    }
    <Components.Logo
      corner={props.page !== steps.LANGUAGE && props.page !== steps.MERCI}
      smaller={props.page === steps.MERCI}
    />
    <Page {...props} />
    <Navigation {...props} />
  </div>
);

export default connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch),
)(App);
