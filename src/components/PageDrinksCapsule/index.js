/**
 * PageDrinksCapsule component
 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ClickOutside from 'react-click-outside';
import './styles.css';

const PageDrinksCapsule = (props) => {
  const selected = props.name === props.selected;

  const onSelect = () =>
    (selected ? props.nextStep() : props.selectItem(props.name));

  const onClose = (e) => {
    if (selected) {
      e.stopPropagation();
      props.deselectItem();
    }
  };

  return (
    <ClickOutside onClickOutside={onClose}>
      <div
        role="button"
        className={classNames('page-drinks-capsule', { selected })}
        onClick={onSelect}
        tabIndex={props.index}
      >
        <div
          className="page-drinks-capsule-image"
          style={{ backgroundImage: `url(capsules/${props.image})` }}
        />
        <button
          type="button"
          onClick={onClose}
          className="page-drinks-capsule-close"
        />
        <div className="page-drinks-capsule-title">
          {props.name}
          <div className="page-drinks-capsule-confirm">
            {props.labelConfirm}
          </div>
        </div>
      </div>
    </ClickOutside>
  );
};

PageDrinksCapsule.propTypes = {
  index: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  labelConfirm: PropTypes.string.isRequired,
  selectItem: PropTypes.func.isRequired,
  deselectItem: PropTypes.func.isRequired,
  selected: PropTypes.string,
};

PageDrinksCapsule.defaultProps = {
  selected: undefined,
};

export default PageDrinksCapsule;
