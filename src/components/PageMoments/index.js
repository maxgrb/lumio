/**
 * PageMoments component
 */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import './styles.css';

const PageMoment = (props) => {
  const selected = props.name === props.selected;

  const onSelect = () =>
    (selected ? props.nextStep() : props.selectItem(props.name));

  return (
    <button
      type="button"
      onClick={onSelect}
      className={classNames('page-moments-item', { selected })}
    >
      {props.name}
      <div className="page-moments-item-confirm">
        {props.lang.confirm}
      </div>
    </button>
  );
};

const PageMoments = (props) => {
  const renderMoment = moment => (
    <PageMoment
      key={moment}
      name={moment}
      {...props}
    />
  );

  return (
    <div className="app-page page-moments">
      <div className="app-page-caption">
        {props.lang.whatIsYourCoffeMoment}
      </div>
      <div className="page-moments-items">
        {props.lang.moments.map(renderMoment)}
      </div>
    </div>
  );
};

PageMoments.propTypes = {
  lang: PropTypes.object.isRequired,
  // selected: PropTypes.string,
};

export default PageMoments;
