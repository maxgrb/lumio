/**
 * ButtonRestart Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const ButtonRestart = props => (
  <button
    type="button"
    className="button-restart"
    onClick={props.onClick}
  >
    {props.label}
  </button>
);

ButtonRestart.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ButtonRestart;
