/**
 * PageDrinks component
 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { PageDrinksCapsule } from '../../components';
import './styles.css';

const DrinksSection = (props) => {
  const renderCapsule = (capsule, index) => (
    <PageDrinksCapsule
      key={capsule.id}
      index={index}
      {...props}
      {...capsule}
    />
  );

  return (
    <div className="page-drinks-section">
      <div className="page-drinks-section-name">
        {props.langName === 'nl' ? props.nameNl : props.nameFr}
      </div>
      <div className="page-drinks-capsules">
        {props.capsules.map(renderCapsule)}
      </div>
    </div>
  );
};

const PageDrinks = (props) => {
  const renderSection = section => (
    <DrinksSection
      key={section.id}
      {...section}
      langName={props.lang.langName}
      selectItem={props.selectItem}
      deselectItem={props.deselectItem}
      nextStep={props.nextStep}
      selected={props.drink}
      labelConfirm={props.lang.confirm}
    />
  );

  return (
    <div className="app-page page-drinks">
      <div className="app-page-caption">
        <div className="page-drinks-username">
          {props.inputName}
        </div>
        {props.lang.chooseYourFavoriteBeverage}
      </div>
      <button
        type="button"
        className="page-drinks-navigation previous"
        disabled={!props.scrolled}
        onClick={props.toggleScroll}
      />
      <button
        type="button"
        className="page-drinks-navigation next"
        disabled={props.scrolled}
        onClick={props.toggleScroll}
      />
      <div className={classNames('page-drinks-sections', { scrolled: props.scrolled })}>
        {props.drinks.map(renderSection)}
      </div>
    </div>
  );
};

PageDrinks.propTypes = {
  inputName: PropTypes.string.isRequired,
  lang: PropTypes.object.isRequired,
  drinks: PropTypes.array.isRequired,
  scrolled: PropTypes.bool.isRequired,
  toggleScroll: PropTypes.func.isRequired,
  // selectItem: PropTypes.func.isRequired,
};

export default PageDrinks;
