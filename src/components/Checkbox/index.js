/**
 * Checkbox Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './styles.css';

const Checkbox = props => (
  <button
    type="button"
    className={classNames('checkbox', { on: props.checked })}
    onClick={props.onChange}
  >
    <div className="checkbox-label">
      {props.label}
    </div>
  </button>
);

Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Checkbox;
