/**
 * PagePersonal component
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from '../../components';
import './styles.css';

const isValidEmail = email => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);

const PagePersonalForm = props => (
  <div className="page-personal-fields">
    <input
      type="text"
      placeholder={props.lang.yourName}
      value={props.inputName}
      onChange={e => props.changeInput('inputName', e.target.value)}
      maxLength={50}
      className="page-personal-input"
    />
    <button
      type="button"
      onClick={props.nextStep}
      disabled={(
        props.inputName.lenght < 2 ||
        !isValidEmail(props.inputEmail) ||
        !props.checkboxTerms
      )}
      className="page-personal-submit"
    >
      {props.lang.ok}
    </button>
    <input
      type="email"
      placeholder={props.lang.email}
      value={props.inputEmail}
      onChange={e => props.changeInput('inputEmail', e.target.value)}
      maxLength={50}
      className="page-personal-input"
    />
  </div>
);

const PagePersonal = props => (
  <div className="app-page page-personal">
    <div className="app-page-caption">
      <p>{props.lang.hello}</p>
      <p>{props.lang.leaveUsFirstName}</p>
    </div>
    <PagePersonalForm {...props} />
    <div className="page-personal-checkboxes">
      <Checkbox
        checked={props.checkboxSubscription}
        onChange={(() => props.toggleCheckbox('checkboxSubscription'))}
        label={props.lang.byCheckingThisBox}
      />
      <Checkbox
        checked={props.checkboxTerms}
        onChange={() => props.toggleCheckbox('checkboxTerms')}
        label={props.lang.iAccept}
      />
    </div>
    <button
      className="link-small"
      onClick={props.togglePopupDisclaimer}
    >
      {props.lang.yourDataIsProcessed}
    </button>
  </div>
);

PagePersonal.propTypes = {
  lang: PropTypes.object.isRequired,
  inputName: PropTypes.string.isRequired,
  inputEmail: PropTypes.string.isRequired,
  changeInput: PropTypes.func.isRequired,
  nextStep: PropTypes.func.isRequired,
  checkboxSubscription: PropTypes.bool.isRequired,
  checkboxTerms: PropTypes.bool.isRequired,
  toggleCheckbox: PropTypes.func.isRequired,
};

export default PagePersonal;
