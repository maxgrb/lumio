/**
 * NavigationLanguage Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const NavigationLanguage = (props) => {
  const onSelect = e => props.onSelect(e.target.value);

  return (
    <div className="navigtation-language">
      <button
        type="button"
        value={props.langs[0]}
        onClick={onSelect}
        className="navigtation-language-button first"
      >
        {props.langs[0]}
      </button>
      <button
        type="button"
        value={props.langs[1]}
        onClick={onSelect}
        className="navigtation-language-button last"
      >
        {props.langs[1]}
      </button>
    </div>
  );
};

NavigationLanguage.propTypes = {
  langs: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default NavigationLanguage;
