/**
 * Logo Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './styles.css';

const Logo = props => (
  <div
    className={classNames('logo', {
      corner: props.corner,
      smaller: props.smaller,
    })}
  />
);

Logo.propTypes = {
  corner: PropTypes.bool,
  smaller: PropTypes.bool,
};

Logo.defaultProps = {
  corner: false,
  smaller: false,
};

export default Logo;
