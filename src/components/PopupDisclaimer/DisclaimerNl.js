/**
 * Popup Disclaimer Nl Component
 */
import React from 'react';

const DisclaimerNl = () => (
  <div>
    <h1>Privacyverklaring</h1>
    <p>Van toepassing sinds 1 mei 2015 laatst bijwerkt op 1 mei 2015
       Nestlé stelt alles in het werk om uw privacy te beschermen en
       ervoor te zorgen dat u uw persoonsgegevens kunt blijven toevertrouwen
       aan Nestlé. Tijdens uw interactie met Nestlé kunt u persoonlijke
       informatie met ons delen, zodat wij u kunnen identificeren als individu
       (zoals uw naam, e-mailadres, thuisadres en telefoonnummer). Dit zijn uw
       &quot;persoonsgegevens&quot;. In deze verklaring (&quot;Privacyverklaring&quot;)
       wordt het volgende uiteengezet:</p>
    <ol>
      <li><a href="#disclaimer1">Reikwijdte en aanvaarding</a></li>
      <li><a href="#disclaimer2">Door Nestlé verzamelde persoonsgegevens</a></li>
      <li><a href="#disclaimer3">Persoonsgegevens van kinderen</a></li>
      <li><a href="#disclaimer4">Waarom Nestlé persoonsgegevens verzamelt en hoe het deze gegevens gebruikt</a></li>
      <li><a href="#disclaimer5">Delen van persoonsgegevens door Nestlé</a></li>
      <li><a href="#disclaimer6">Uw rechten</a></li>
      <li><a href="#disclaimer7">Veiligheid en bewaren van gegevens</a></li>
      <li><a href="#disclaimer8">Hoe contact opnemen</a></li>
    </ol>

    <h2 id="disclaimer1">1. Reikwijdte en aanvaarding van deze Privacyverklaring</h2>
    <p>Deze Privacyverklaring is van toepassing op de persoonsgegevens die we over u
      verzamelen met als doel u onze producten en diensten aan te bieden. Door gebruik
      te maken van de Nestlé sites (zie definitie hieronder) of door ons uw persoonsgegevens
      mee te delen, aanvaardt u de praktijken die in deze Privacyverklaring worden beschreven.
      Als u niet akkoord gaat met deze Privacyverklaring, gelieve dan geen gebruik te maken
      van de Nestlé sites (zie definitie hieronder) en ons geen persoonsgegevens mee te delen.
      Nestlé behoudt zich het recht voor deze Privacyverklaring te allen tijde te wijzigen.
      We raden u aan deze Privacyverklaring regelmatig te raadplegen, zodat u zeker op de
      hoogte bent van eventuele wijzigingen en steeds weet hoe uw persoonsgegevens kunnen
      worden gebruikt.</p>

    <h2 id="disclaimer2">2. Door Nestlé verzamelde persoonsgegevens</h2>
    <p>Nestlé kan persoonsgegevens verzamelen via verschillende bronnen, waaronder:</p>
    <ul>
      <li>Online en elektronische interacties met ons, onder andere via websites van Nestlé,
        mobiele applicaties, sms-programma’s of &quot;branded&quot; pagina’s of applicaties
        van Nestlé op sociale netwerken van derden (zoals Facebook) (Nestlé sites);</li>
      <li>Offline interacties met ons, onder andere via direct-marketingacties, gedrukte
        registratiekaarten, deelname aan wedstrijden en contacten via de callcenters van de
        Nestlé klantendienst; en</li>
      <li>Uw interactie met online gerichte inhoud (zoals advertenties) die door Nestlé, of
        door dienstverleners namens Nestlé, wordt aangeboden via applicaties of websites van
        derden.</li>
    </ul>
    <h3>2.1 Gegevens die u rechtstreeks aan ons verstrekt</h3>
    <p>Dit zijn gegevens die u met uw toestemming rechtstreeks aan ons verstrekt voor een
      specifiek doel, waaronder:</p>
    <ul>
      <li>Persoonlijke contactinformatie, met inbegrip van alle informatie die Nestlé
        nodig heeft om persoonlijk met u contact op te nemen (zoals uw naam, thuis- of
        e-mailadres, en telefoonnummer);</li>
      <li>Demografische informatie, waaronder uw geboortedatum, leeftijd, geslacht, locatie
        (postcode, gemeente/stad, provincie en geolocatie), favoriete producten, hobby’s,
        interesses, en informatie over uw gezin of levensstijl;</li>
      <li>Betaalinformatie, onder andere om aankopen te doen (zoals kredietkaartnummer,
        vervaldatum, factuuradres);</li>
      <li>Inloggegevens, met inbegrip van informatie die vereist is om een gebruikersaccount
        te creëren bij Nestlé (zoals login-ID/e-mailadres, gebruikersnaam, wachtwoord en
        veiligheidsvraag en -antwoord);</li>
      <li>Consumentenfeedback, waaronder informatie die u deelt met Nestlé over uw ervaring
        met Nestlé producten en diensten (zoals uw opmerkingen en suggesties, getuigenissen
        en andere feedback in verband met Nestlé producten); en</li>
      <li>Inhoud die door de consument is gegenereerd, waaronder inhoud (zoals foto’s,
        video’s en persoonlijke verhalen) die u creëert en vervolgens deelt met Nestlé
        (en mogelijk anderen) door deze te uploaden naar een Nestlé site.</li>
    </ul>
    <h3>2.2  Gegevens die we verzamelen wanneer u een Nestlé site gebruikt.</h3>
    <p>We gebruiken cookies en andere traceringstechnologieën die bepaalde informatie
      verzamelen wanner u een Nestlé site bezoekt. Zie onze Cookieverklaring voor meer
      informatie over deze technologieën en uw rechten in dit verband.</p>
    <h3>2.3  Gegevens verzameld uit andere bronnen</h3>
    <p>We kunnen informatie over u verzamelen via andere legitieme bronnen om u onze
      producten en diensten aan te bieden. Voorbeelden van dergelijke bronnen zijn externe
      gegevensverzamelaars, promotionele partners van Nestlé, openbare bronnen en sociale
      netwerken van derden. Dergelijke informatie kan het volgende omvatten:</p>
    <ul>
      <li>persoonlijke contactinformatie; en</li>
      <li>alle persoonsgegevens die deel uitmaken van uw profiel bij een sociaal netwerk
        van derden (bv. Facebook) en door dat sociale netwerk met ons mogen worden gedeeld
        (zoals uw naam, e-mailadres, geslacht, verjaardag, gemeente/stad, profielfoto,
        gebruiksnaam en vriendenlijst). Meer informatie over de gegevens die we kunnen
        verzamelen vindt u op de website van het betreffende sociale netwerk.</li>
      <li>We kunnen ook persoonsgegevens in ons bezit krijgen wanneer we andere bedrijven
        overnemen.</li>
    </ul>

    <h2 id="disclaimer3">3. Persoonsgegevens van kinderen</h2>
    <p>Nestlé vraagt of verzamelt nooit bewust persoonsgegevens van kinderen jonger dan
      12 jaar. Als Nestlé ontdekt dat het per ongeluk persoonsgegevens heeft verzameld
      van een kind jonger dan 12 jaar, dan zal het de persoonsgegevens van dat kind zo
      snel als redelijkerwijs mogelijk verwijderen uit zijn bestanden. Nestlé kan evenwel
      persoonsgegevens van kinderen jonger dan 12 jaar rechtstreeks via een ouder of
      voogd verzamelen, en dus met hun uitdrukkelijke toestemming.</p>

    <h2 id="disclaimer4">4. Waarom Nestlé persoonsgegevens verzamelt en hoe het deze gegevens gebruikt</h2>
    <p>Nestlé verzamelt en gebruikt persoonsgegevens uitsluitend voor het doel waarvoor
      de gegevens werden verkregen. Nestlé kan uw persoonsgegevens gebruiken voor alle
      of sommige van de volgende doeleinden:</p>
    <ul>
      <li>Bestellingen: om uw bestellingen te verwerken en te verzenden en u te informeren
        over de status ervan. Merk op dat er veel webshops zijn die Nestlé producten verkopen
        maar die niet worden gecontroleerd of beheerd door Nestlé. We raden aan dat u hun
        beleidslijnen leest, inclusief deze over privacy, alvorens producten op die websites
        te kopen.</li>
      <li>Onderhoud van uw account: om uw accounts bij Nestlé te creëren en te onderhouden,
        met inbegrip van het aanbieden van getrouwheids- of beloningsprogramma’s die verbonden
        zijn met uw account.</li>
      <li>Consumentendienst: om u als consument beter te bedienen, waaronder antwoorden op uw
        vragen, klachten en algemene feedback over onze producten. Onze consumentendienst kan
        worden aangeboden via verschillende communicatievormen, zoals e-mail, brief, telefoon
        en online chat.</li>
      <li>Betrokkenheid van de consument: om u actiever te betrekken bij onze producten en
        diensten. Het kan daarbij gaan om het gebruik of de publicatie van inhoud die door de
        consument is gegenereerd.</li>
      <li>Personalisering: Nestlé kan persoonsgegevens die via de ene bron zijn verzameld
        (bv. een website) combineren met gegevens die afkomstig zijn van een andere bron (bv.
        een offline event). Zo krijgt Nestlé een vollediger beeld van u als consument, waarmee
        Nestlé u dan weer beter en persoonlijker van dienst kan zijn, onder andere met betrekking
        tot het volgende:
        <ul>
          <li>Websites: om uw ervaring op onze websites te verbeteren en te personaliseren,
            door gebruik te maken van gegevens zoals inloginformatie, technische
            computerinformatie en/of informatie over eerder gebruik van de website;</li>
          <li>Producten: om de producten van Nestlé te verbeteren, beter af te stemmen op uw
            behoeften en nieuwe productideeën te bedenken. Dit omvat het gebruik van
            demografische informatie, informatie over consumentenprofilering en
            consumentenfeedback; en</li>
          <li>Reclame gebaseerd op interesses: om u advertenties aan te bieden die zijn
            afgestemd op uw interesses. Een van de manieren waarop Nestlé dit doet, is door
            de activiteiten of verzamelde informatie op de Nestlé sites te koppelen aan
            gegevens die op websites van derden over u zijn verzameld. Dit soort reclame
            staat ook bekend als &quot;gerichte reclame&quot; of &quot;online behavioural
            advertising&quot;. Een dergelijke personalisering gebeurt doorgaans via cookies
            of gelijkaardige technologieën.</li>
        </ul>
      </li>
      <li>Reclameberichten: om reclameberichten te versturen als u zich hebt opgegeven voor
        het ontvangen van dergelijke berichten (met inbegrip van informatie over Nestlé,
        zijn producten en diensten, wedstrijden en promoties). Deze berichten kunnen worden
        verspreid op elektronische wijze (bv. sms, e-mail en online reclame) en via de post.
        Als u zich opgeeft om sms-berichten te ontvangen, is het beleid van uw mobiele
        serviceprovider voor het ontvangen van sms-berichten van toepassing, waarvoor kosten
        kunnen worden aangerekend.</li>
      <li>Sociale functies: om u een aantal sociale functies aan te bieden, waaronder:
        <ul>
          <li>Community-functies op een Nestlé site<br />
          Wanneer u een Nestlé site met een community-functie bezoekt en recepten, foto’s,
          video’s, illustraties of andere inhoud deelt, kan Nestlé de persoonsgegevens die
          u op dergelijke websites deelt, gebruiken en weergeven.</li>
          <li>Virale websitefuncties<br />
          Nestlé kan uw persoonsgegevens gebruiken om u virale websitefuncties aan te
          bieden, zoals een &quot;tell-a-friend&quot;-programma, waarmee u bepaalde nieuwsberichten,
          productinformatie, promoties en andere inhoud kunt delen met familie en vrienden.
          Hiervoor moet gewoonlijk persoonlijke contactinformatie (zoals namen en e-mailadressen)
          worden verzameld en gebruikt, zodat het bericht / de inhoud in kwestie kan worden
          bezorgd aan de ontvangers.</li>
          <li>Sociale netwerken van derden<br />
          Nestlé kan uw persoonsgegevens gebruiken wanneer u socialenetwerk-functies van
          derden gebruikt, zoals &quot;Facebook Connect&quot; of &quot;Facebook Like&quot;.
          Deze functies kunnen geïntegreerd zijn in de Nestlé sites, onder andere met het
          oog op de organisatie van wedstrijden en om u de mogelijkheid te bieden inhoud te
          delen met vrienden. Als u deze functies gebruikt, is het mogelijk dat Nestlé
          bepaalde persoonsgegevens over u kan verkrijgen via het sociale netwerk in kwestie.
          Meer informatie over hoe deze functies werken en de profielgegevens die Nestlé over
          u kan verkrijgen, vindt u terug op de website van het betreffende sociale netwerk.</li>
          <li>Andere specifieke doeleinden<br />
          We kunnen uw persoonsgegevens gebruiken voor andere specifieke bedrijfsdoeleinden,
          zoals de dagelijkse werking en veiligheid van de Nestlé sites, om demografische
          studies of audits uit te voeren en om met u contact op te nemen voor consumentenonderzoek.
          </li>
        </ul>
      </li>
    </ul>

    <h2 id="disclaimer5">5. Delen van persoonsgegevens door Nestlé</h2>
    <p>Nestlé deelt uw persoonsgegevens niet met derden die uw gegevens willen gebruiken voor
      direct marketing, tenzij u daarvoor uw specifieke toestemming hebt gegeven.<br />
      Nestlé kan uw persoonsgegevens ook voor andere doeleinden delen met derden, maar alleen
      in de volgende gevallen:</p>
    <h3>5.1 Verwante bedrijven</h3>
    <p>Nestlé kan uw persoonsgegevens voor legitieme bedrijfsdoeleinden (zoals bijvoorbeeld
      om bestellingen te verwerken, klachtenbehandeling, … ) meedelen aan zijn verwante
      bedrijven of dochterondernemingen.</p>
    <h3>5.2 Dienstverleners</h3>
    <p>Nestlé kan een beroep doen op dienstverleners, vertegenwoordigers of contractanten
      om diensten te verlenen namens Nestlé, met inbegrip van het aanbieden van de Nestlé
      sites en de diensten die voor u beschikbaar zijn. Het is mogelijk dat deze externe
      partijen bij het verlenen van deze diensten toegang krijgen tot uw persoonsgegevens
      of deze moeten verwerken.</p>
    <p>Nestlé eist door middel van een schriftelijke overeenkomst van dergelijke externe
      partijen, die gevestigd kunnen zijn in een ander land dan het land waarin u de Nestlé
      site of dienst hebt gebruikt, dat zij zich houden aan alle relevante wetgeving inzake
      gegevensbescherming en beveiligingsvereisten met betrekking tot uw persoonsgegevens.</p>
    <h3>5.3 Partners en gemeenschappelijke promoties</h3>
    <p>Nestlé kan gemeenschappelijke of medegesponsorde programma’s of promoties uitvoeren
      met een ander bedrijf en, in het kader van uw betrokkenheid in de activiteit, uw
      persoonsgegevens verzamelen en gebruiken.</p>
    <p>Uw persoonsgegevens zullen uitsluitend worden gedeeld met een ander bedrijf als u
      zich hebt opgegeven om rechtstreeks van dat bedrijf informatie te ontvangen. Nestlé
      raadt u aan de privacyverklaring van dergelijke bedrijven te lezen alvorens uw
      persoonsgegevens te delen. Als u niet wilt dat uw persoonsgegevens worden verzameld
      door of gedeeld met een ander bedrijf dan Nestlé, kunt u er altijd voor kiezen om
      niet deel te nemen aan dergelijke activiteiten. Als u zich opgeeft om berichten van
      een dergelijk bedrijf te ontvangen, denk er dan aan dat u altijd het recht hebt om
      zich uit te schrijven en u in dat geval rechtstreeks contact dient op te nemen met
      dat bedrijf.</p>
    <h3>5.4 Wettelijke vereisten en bedrijfsoverdracht</h3>
    <p>Nestlé kan uw persoonsgegevens openbaar maken als het daartoe wettelijk wordt
      verplicht of als Nestlé te goeder trouw oordeelt dat een dergelijke openbaarmaking
      redelijkerwijs noodzakelijk is om te voldoen aan wettelijke procedures of om te
      reageren op eventuele vorderingen.</p>
    <p>In geval van een volledige of gedeeltelijke fusie met of een volledige of
      gedeeltelijke overname van Nestlé door een ander bedrijf, zou de overnemer
      toegang krijgen tot de informatie die door die entiteit van Nestlé wordt bijgehouden.
      Die informatie kan persoonsgegevens bevatten.</p>

    <h2 id="disclaimer6">6. Uw rechten</h2>
    <h3>6.1 Recht om zich uit te schrijven voor reclameberichten</h3>
    <p>U hebt het recht om zich uit te schrijven voor reclameberichten over Nestlé
      (&quot;opt-out&quot;) en kunt dat doen door:</p>
    <ul>
      <li>de instructies te volgen in het betreffende reclamebericht;</li>
      <li>als u een account hebt bij Nestlé, beschikt u wellicht over de mogelijkheid om
        uw accountgegevens te bewerken en uw voorkeuren in verband met reclameberichten
        te wijzigen; of</li>
      <li>met ons contact op te nemen. Merk op dat zelfs als u doorgeeft geen
        reclameberichten meer te willen ontvangen, u nog steeds administratieve berichten
        van Nestlé kunt ontvangen, zoals bestellingsbevestigingen en berichten over uw
        accountactiviteiten (zoals accountbevestigingen en wachtwoordwijzigingen).</li>
    </ul>
    <h3>6.2 Toegang en rechtzetting</h3>
    <p>U hebt het recht om toegang tot uw persoonsgegevens te vragen. U kunt ons daartoe
      een verzoek sturen. Als Nestlé geen toegang kan geven tot uw persoonsgegevens, zal
      het u laten weten waarom dat niet mogelijk is.</p>
    <p>U hebt ook het recht om Nestlé te vragen onjuistheden in uw persoonsgegevens te
      verbeteren. Als u bij Nestlé een account hebt voor een Nestlé site, kunt u dat
      doorgaans doen via het onderdeel &quot;uw account&quot; of &quot;uw profiel&quot; op
      de Nestlé site (indien beschikbaar). Zo niet kunt u ons een verzoek sturen om uw
      gegevens te corrigeren.
    </p>

    <h2 id="disclaimer7">7. Veiligheid en bewaren van gegevens</h2>
    <h3>7.1 Gegevensbeveiliging</h3>
    <p>Om uw persoonsgegevens te beveiligen, heeft Nestlé een aantal maatregelen
      ingevoerd, waaronder:</p>
    <ul>
      <li>Beveiligde bedrijfsomgeving: Nestlé slaat uw gegevens op in beveiligde
        bedrijfsomgevingen die alleen toegankelijk zijn voor werknemers,
        vertegenwoordigers en contractanten van Nestlé op &quot;need-to-know&quot;-basis.
        Nestlé volgt in dit verband ook de normen die in de sector algemeen zijn aanvaard.</li>
      <li>Versleuteling van betaalgegevens: Nestlé maakt gebruik van versleuteling die
        standaard in de sector wordt toegepast om gevoelige financiële informatie te
        beschermen, zoals kredietkaartgegevens die over het internet worden verstuurd
        (bv. wanneer u betalingen doet in een webshop van Nestlé).</li>
      <li>Voorafgaande authenticatie voor accounttoegang: Nestlé verifieert de
        identiteit van zijn geregistreerde consumenten (login-ID en wachtwoord)
        alvorens ze toegang krijgen tot of wijzigingen kunnen aanbrengen in hun
        account. De bedoeling hiervan is toegang door onbevoegden te voorkomen.<br />
        Merk op dat deze beveiligingen niet van toepassing zijn op persoonsgegevens
        die u deelt op openbare plaatsen, zoals community-websites.</li>
    </ul>
    <h3>7.1 Bewaring</h3>
    <p>Nestlé zal uw persoonsgegevens niet langer dan noodzakelijk bewaren voor het
      vermelde doel, rekening houdend met onze behoefte om vragen te beantwoorden en
      problemen op te lossen, diensten te verbeteren en nieuwe diensten aan te bieden,
      en de wettelijke voorschriften na te leven.</p>
    <p>Dat betekent dat we uw persoonsgegevens na uw laatste interactie met Nestlé
      gedurende een redelijke periode kunnen bijhouden. Wanneer de persoons-gegevens
      die we verzamelen niet langer vereist zijn, zullen we deze op een veilige manier
      vernietigen of verwijderen.</p>

    <h2 id="disclaimer8">8. Contact opnemen</h2>
    <p>Nestlé treedt op als &quot;verantwoordelijke voor de verwerking&quot; voor de
      persoonsgegevens die het verwerkt in het kader van deze Privacyverklaring..
      Als u vragen of opmerkingen hebt over deze Privacyverklaring of de manier waarop
      Nestlé persoonsgegevens verzamelt, neem dan met ons contact op via:</p>
    <p>Telefoon op +32 (0)2 529 55 25<br />
    E-mail naar consum@be.nestle.com<br />
    Brief naar Nestlé Belgilux NV, Consumentendienst, Birminghamstraat 221 1070 Brussel</p>
    <p>Copyright © mei 2015 Nestlé</p>

  </div>
);

export default DisclaimerNl;
