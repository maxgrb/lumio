/**
 * Popup Disclaimer Fr Component
 */
import React from 'react';

const DisclaimerFr = () => (
  <div>
    <h1>Déclaration de confidentialité</h1>
    <p>Prise d’effet le 01 mai 2015 dernière mise à jour le 01 mai 2015
      Nestlé s’engage à préserver la confidentialité de vos données personnelles
      afin que vous puissiez continuer à nous les communiquer en toute confiance.
      Lorsque vous interagissez avec nous, il se peut que vous nous communiquiez des
      informations personnelles qui nous permettent de vous identifier en tant que
      personne (ex. nom, courriel, adresse, numéro de téléphone). Ces informations
      sont appelées des &quot;données personnelles&quot;.
      La présente déclaration (&quot;Déclaration de Confidentialité&quot;) apporte
      des précisions sur :
    </p>
    <ol>
      <li><a href="#disclaimer1">Le périmètre d’application et l’acceptation</a></li>
      <li><a href="#disclaimer2">Les données personnelles collectées par Nestlé</a></li>
      <li><a href="#disclaimer3">Les données personnelles des enfants</a></li>
      <li><a href="#disclaimer4">Les raisons pour lesquelles Nestlé collecte des données personnelles et
        la manière dont celles-ci sont utilisées</a></li>
      <li><a href="#disclaimer5">Le partage des données personnelles par Nestlé</a></li>
      <li><a href="#disclaimer6">Vos droits</a></li>
      <li><a href="#disclaimer7">La sécurité et la conservation des données</a></li>
      <li><a href="#disclaimer8">Comment nous contacter</a></li>
    </ol>

    <h2 id="disclaimer1">1. Périmètre d’application et acceptation de cette Déclaration de Confidentialité</h2>
    <p>La présente Déclaration de Confidentialité s’applique aux données personnelles
      que nous collectons à votre sujet aux fins de vous fournir les services et les
      produits que nous proposons.</p>
    <p>En utilisant les Sites de Nestlé (comme définis ci-dessous) ou en nous communiquant
      vos données personnelles, vous acceptez les pratiques décrites dans la présente
      Déclaration de Confidentialité. Si vous n’acceptez pas les dispositions de la
      présente Déclaration de Confidentialité, veuillez ne pas utiliser les Sites de
      Nestlé (comme définis ci-dessous) et ne pas nous communiquer vos données personnelles.</p>
    <p>Nestlé se réserve le droit d’apporter à tout moment des changements à la présente
      Déclaration de Confidentialité. Nous vous recommandons de consulter régulièrement cette
      Déclaration de Confidentialité afin de vous informer de tout changement et de la manière
      dont vos données personnelles peuvent être utilisées.</p>

    <h2 id="disclaimer2">2. Données collectées par Nestlé</h2>
    <p>Nestlé peut collecter des données personnelles à votre sujet auprès d’une diversité
      de sources, ainsi que par le biais des moyens suivants :</p>
    <ul>
      <li>Les interactions électroniques et en ligne avec nous, dont celles réalisées via
        les sites web, les applications mobiles, les programmes de messagerie textuelle de
        Nestlé, ou les pages ou les applications portant notre marque sur les réseaux sociaux
        externes (ex. Facebook) (Sites de Nestlé );</li>
      <li>Les interactions hors ligne avec nous, parmi lesquelles les campagnes de marketing
        direct, les cartes d’inscription imprimées, les participations à des concours et les
        contacts établis par le biais des centres d’appels grand public de Nestlé ; et</li>
      <li>Votre interaction avec les contenus ciblés en ligne (comme les publicités) que Nestlé
        ou les prestataires de services agissant pour notre compte vous fournissent par le biais
        de sites web ou d’applications externes.</li>
    </ul>
    <h3>2.1 Données que vous nous fournissez directement</h3>
    <p>Il s’agit des données que vous nous fournissez, avec votre consentement et à des fins
      spécifiques, comme :</p>
    <ul>
      <li>Les informations personnelles de contact, y compris toutes les informations
        permettant à Nestlé de vous contacter en personne (ex. nom, adresse postale ou
        électronique et numéro de téléphone) ;</li>
      <li>Les informations démographiques, comme votre date de naissance, votre âge, votre sexe,
        votre lieu de résidence (ex. code postal, ville, pays et géolocalisation), vos produits
        favoris, vos intérêts personnels et les informations concernant votre ménage ou votre
        mode de vie ;</li>
      <li>Les informations de paiement, y compris celles nécessaires pour effectuer des achats
        (ex. numéro de carte de crédit, date d’expiration, adresse de facturation) ;</li>
      <li>Les informations d’accès au compte, y compris toutes les informations nécessaires pour
        créer un compte d’utilisateur auprès de Nestlé (ex. identifiant/courriel d’ouverture de
        session, nom d’utilisateur, mot de passe et questions/réponses de sécurité) ;</li>
      <li>Les retours d’information des clients, y compris les informations que vous partagez
        avec Nestlé concernant votre expérience d’utilisateur des produits et des services de
        Nestlé (ex. vos commentaires et vos suggestions, vos témoignages et toutes vos autres
        réactions en rapport avec les produits de Nestlé) ; et</li>
      <li>Les contenus générés par les consommateurs, y compris tous les contenus (ex. photos,
        vidéos et histoires personnelles) que vous créez, puis que vous partagez avec Nestlé
        (et peut-être d’autres parties) en les téléchargeant sur un Site de Nestlé.</li>
    </ul>
    <h3>2.2 Données que nous collectons lorsque vous interagissez avec les Sites de Nestlé</h3>
    <p>Nous utilisons des cookies et d’autres technologies de suivi pour collecter certains
      types d’informations lorsque vous interagissez avec les Sites de Nestlé. Veuillez vous
      référer à notre Avis relatif aux Cookies pour y trouver des informations sur ces technologies
      et sur vos droits en rapport avec celles-ci.</p>
    <h3>2.3 Données collectées auprès d’autres sources</h3>
    <p>Il se peut que nous collections des informations à votre sujet auprès d’autres sources
      légitimes aux fins de vous fournir nos produits et nos services. Parmi ces sources, on peut
      citer les agrégateurs externes de données, les partenaires de Nestlé dans des opérations
      promotionnelles, les sources publiques et les sites des réseaux sociaux externes. Parmi ces
      informations, il peut y avoir :</p>
    <ul>
      <li>Des informations personnelles de contact ; et</li>
      <li>Toutes les données personnelles qui font partie de votre profil sur un réseau social
        externe (ex. Facebook) et que vous avez autorisées ce réseau social externe à partager
        avec nous (ex. nom, adresse électronique, sexe, date de naissance, ville, photo du profil,
        identifiant d’utilisateur, liste d’amis). Vous pourrez en savoir plus sur les données
        que nous pouvons obtenir à votre sujet en visitant le site du réseau social externe
        concerné.</li>
    </ul>
    <p>Il se peut également que nous recevions des données personnelles vous concernant lorsque
      nous rachetons d’autres sociétés.</p>

    <h2 id="disclaimer3">3. Les données personnelles des enfants</h2>
    <p>Nestlé ne recherche ni ne collecte délibérément des données personnelles au sujet d’enfants
      d’un âge inférieur à 12 ans. Si Nestlé découvre qu’elle a accidentellement collecté des
      données personnelles concernant un enfant de moins de 12 ans, elle effacera de ses registres
      les données personnelles de cet enfant dès que cela lui sera raisonnablement possible.
      Toutefois, Nestlé pourra collecter des données personnelles concernant des enfants de
      moins de 12 ans directement auprès du parent ou du gardien de l’enfant, et donc avec
      leur permission explicite.</p>

    <h2 id="disclaimer4">4. Pourquoi Nestlé collecte des données personnelles et comment elle les utilise</h2>
    <p>Nestlé collecte et utilise les données personnelles dans la seule mesure nécessaire
      aux fins pour lesquelles elles ont été obtenues. Nestlé peut utiliser vos données
      personnelles à l’une des fins suivantes :</p>
    <ul>
      <li>Les commandes – pour traiter et expédier vos commandes et vous informer de l’état
        d’avancement de vos commandes. Veuillez noter qu’il existe un grand nombre de sites
        d’e-commerce qui vendent des produits Nestlé mais qui ne sont ni contrôlés ni exploités
        par Nestlé. Nous vous recommandons de lire attentivement leurs politiques, en particulier
        celles relatives à la confidentialité, avant d’effectuer des achats sur ces sites web.</li>
      <li>La maintenance du compte – pour créer et gérer les comptes que vous avez chez nous, y
        compris l’administration de tout compte de fidélité ou de gratification qui pourrait être
        associé à votre compte.</li>
      <li>Le service à la clientèle – pour vous offrir un service d’assistance à la clientèle,
        incluant la réponse à vos demandes de renseignements, à vos réclamations et, de manière
        générale, aux réactions concernant nos produits. Le service à la clientèle peut être assuré
        par le biais de diverses formes de communication, parmi lesquelles les courriels, les
        lettres, les appels téléphoniques et les conversations en ligne.</li>
      <li>La sensibilisation des clients – afin de mieux vous sensibiliser et vous intéresser à
        nos produits et à nos services. Ceci peut impliquer l’utilisation ou la publication de
        contenus générés par les consommateurs.</li>
      <li>La personnalisation - Nestlé peut combiner des données personnelles à votre sujet
        obtenues auprès de sources différentes (ex. un site web et un événement hors ligne).
        Ceci permet à Nestlé de disposer d’une image plus complète de vous, le consommateur,
        ce qui permet à son tour à Nestlé de mieux vous servir, en personnalisant certains des
        aspects suivants :
        <ul>
          <li>Les sites web - pour améliorer et personnaliser votre expérience sur les sites web,
            en utilisant les données comme les informations d’accès au compte, les informations
            techniques de l’ordinateur et/ou les informations relatives aux utilisations précédentes
            du site web ;</li>
          <li>Les produits - pour améliorer les produits de Nestlé, les adapter à vos besoins et
            générer de nouvelles idées de produits. Ceci inclut l’utilisation des données
            démographiques, des informations de profilage des consommateurs et les réactions
            des consommateurs ; et</li>
          <li>La publicité ciblée - afin de vous apporter des publicités centrées sur vos intérêts.
            L’une des manières dont Nestlé procède est de mettre en correspondance les activités
            ou les informations collectées sur les Sites de Nestlé avec les données collectées à
            votre sujet sur les sites externes (c’est-à-dire, procéder à un appariement des
            données). Ce type de publicité est également appelé &quot;publicité comportementale
            en ligne&quot; ou &quot;publicité ciblée&quot;. Ce genre de personnalisation est
            habituellement réalisée par le biais de cookies ou de technologies similaires.</li>
        </ul>
      </li>
      <li>Les communications de marketing – pour vous apporter les communications de marketin
         dans les cas où vous avez choisi de recevoir de telles communications (y compris des
         informations au sujet de Nestlé, ses produits et ses services, ses concours et ses
         promotions). Celles-ci peuvent partagées par le biais de moyens électroniques (ex. SMS,
         courriels et publicités en ligne) et par la poste.<br />
         Si vous choisissez de recevoir des SMS, la politique de votre prestataire de services
         mobiles concernant les SMS s’appliquera, et cela peut entraîner des frais.
      </li>
      <li>Les fonctions sociales – Pour vous offrir un certain nombre de fonctions sociales,
        parmi lesquelles :
        <ul>
          <li>Les fonctions communautaires de site web<br />sur un Site de Nestlé Lorsque vous vous
            rendez sur un Site de Nestlé disposant d’une fonction communautaire et que vous y
            téléchargez ou y partagez des recettes, des photos, des vidéos, des œuvres graphiques
            ou tout autre contenu,Nestlé peut utiliser et afficher les données personnelles que
            vous partagez sur de tels sites.</li>
          <li>Les fonctions virales de site web<br />Nestlé peut utiliser vos données personnelles
          pour mettre à votre disposition des fonctions virales de site web, comme les programmes
          de recommandation aux amis (&quot;tell-a-friend&quot;), par le biais desquels il vous
          est possible de partager certaines nouvelles, informations de produit, promotions ou
          autres contenus avec votre famille et vos amis. Ceci demande habituellement que les
          informations personnelles de contact soient collectées et utilisées (ex. les noms et
          les courriels) afin que le message ou contenu sélectionné une seule fois soit livré à
          ses destinataires.
          </li>
          <li>L’usage de réseaux sociaux externes<br />Nestlé peut utiliser vos données
          personnelles résultant de votre usage de fonctions de réseau social externes,
          comme &quot;Facebook Connect&quot; ou &quot;Facebook Like&quot;. Il se peut que ces
          fonctions soient intégrées sur les Sites de Nestlé à des fins comme la participation
          à des concours et le partage de contenus avec des amis.<br />Si vous utilisez ces
          fonctions, il est possible que Nestlé soit en mesure d’obtenir certaines données
          personnelles à votre sujet à partir de vos informations sur les réseaux sociaux.
          Vous pourrez en savoir plus sur la manière dont ces fonctions agissent et sur les
          données de profil que Nestlé peut obtenir à votre sujet en vous rendant sur le site
          web du réseau social externe concerné.</li>
          <li>D’autres fins spécifiques<br />Il se peut que nous utilisions vos données
          personnelles à d’autres fins professionnelles spécifiques, comme l’exploitation
          courante et la sécurité des Sites de Nestlé, pour réaliser des études démographiques
          ou des audits, ainsi que pour vous contacter pour réaliser des études de marché auprès
          des consommateurs.</li>
        </ul>
      </li>
    </ul>

    <h2 id="disclaimer5">5. Le partage des données personnelles par Nestlé</h2>
    <p>Nestlé ne partage aucunement vos données personnelles avec des parties tierces qui
      ont l’intention de les utiliser à des fins de marketing direct, à moins que vous n’ayez
      spécifiquement donné votre accord à cette fin. Nestlé peut également partager vos données
      personnelles avec des parties tierces à d’autres buts, mais uniquement dans les
      circonstances suivantes :</p>
    <h3>5.1 Sociétés filiales</h3>
    <p>Il se peut que Nestlé fournisse vos données personnelles à ses affiliés ou à des sociétés
      partenaires à des fins professionnelles légitimes (par exemple, te traitement des commandes,
      le traitement des plaintes,…)</p>
    <h3>5.2 Prestataires de services</h3>
    <p>Nestlé peut engager des prestataires de services, des agents ou des sous-traitants afin
      qu’ils assurent les services pour son compte, et ceci inclut la gestion des Sites de Nestlé
      et des services qui vous sont proposés. Il est possible que ces parties tierces accèdent à
      ou traitent d’une manière quelconque vos données personnelles lors de l’exécution de cette
      prestation de services.</p>
    <p>Nestlé exige par le biais d’une convention écrite de ces parties tierces, qui peuvent
      être basées hors du pays depuis lequel vous avez accédé au Site ou au service de Nestlé,
      qu’elles respectent toutes les lois afférentes à la protection des données et toutes les
      exigences de sécurité applicables à vos données personnelles.</p>
    <h3>5.3 Partenaires et promotions communes</h3>
    <p>Il se peut que Nestlé participe à un programme ou à une promotion organisée en partenariat
      ou commanditée conjointement avec une autre société et que, en conséquence de votre
      participation à cette activité, nous collections et utilisions vos données personnelles.</p>
    <p>Vos données personnelles ne seront partagées avec une autre société que si vous avez
      choisi de recevoir des informations directement de cette société. Nestlé vous recommande
      de lire attentivement la déclaration de confidentialité de cette société avant de partager
      vos données personnelles. Si vous ne voulez pas que vos données personnelles soient
      collectées par ou partagées avec une société autre que Nestlé, vous pouvez toujours
      choisir de ne pas participer à cette activité. Si vous choisissez de recevoir des
      communications d’une telle société, rappelez-vous que vous avez toujours le droit de refuser,
      et il vous faudra alors contacter directement cette société pour cela.</p>
    <h3>5.4 Obligations légales et transferts commerciaux</h3>
    <p>Nestlé peut divulguer vos données personnelles si elle en est obligée par la loi ou si elle
      estime de bonne foi que cette divulgation légale est nécessaire, dans la mesure du
      raisonnable, pour respecter des processus légaux ou répondre à des revendications
      quelconques.</p>
    <p>En cas de fusion totale ou partielle avec, ou de rachat total ou partiel de Nestlé par une
      autre société, l’acheteur aurait alors accès aux informations conservées par cette division
      de Nestlé, qui pourraient inclure des données personnelles.</p>

    <h2 id="disclaimer6">6. Vos droits</h2>
    <p>Vous avez le droit de refuser de recevoir les communications de marketing concernant
      Nestlé (&quot;opt-out&quot;) et vous pouvez le faire en :</p>
    <ol>
      <li>Suivant les instructions de refus dans la communication de marketing concernée ;</li>
      <li>si vous disposez d’un compte auprès de Nestlé, il est possible que vous ayez l’option de
        modifier vos préférences d’acceptation et de refus dans la section d’édition de compte
        appropriée du compte ; ou</li>
      <li>Nous contactant .<br />
      Veuillez noter que même si vous choisissez de ne pas recevoir de communications de marketing
      de notre part, il se peut que nous continuions à vous envoyer des communications
      administratives, comme des confirmations de commande, des notifications concernant les
      activités survenues sur votre compte (ex. confirmations de compte, changements de mot de
      passe, etc.).</li>
      <li>Accès et rectification Vous avez le droit de demander d’accéder à vos données
        personnelles. Vous pouvez nous envoyer une demande d’accès. Si Nestlé n’est pas en mesure
        de vous donner accès à vos données personnelles, elle vous en donnera les raisons.<br />
        Vous avez également le droit de demander que Nestlé rectifie les inexactitudes dans
        vos données personnelles. Si vous disposez d’un compte auprès de Nestlé pour l’accès
        à un Site de Nestlé, il est habituellement possible de procéder à cette rectification
        en accédant à la ou les sections &quot;Votre compte&quot; ou &quot;Votre profil&quot;
        sur le Site de Nestlé (si elles sont disponibles). Autrement, vous pouvez nous
        envoyer une demande de rectification de vos données.</li>
    </ol>

    <h2 id="disclaimer7">7. La sécurité et la conservation des données</h2>
    <h3>a) Sécurité des données</h3>
    <p>De manière à préserver la sécurité de vos données personnelles, Nestlé a mis en œuvre
      un certain nombre de mesures de sécurité, parmi lesquelles :</p>
    <ul>
      <li>Des environnements d’exploitation sécurisés - Nestlé entrepose vos données dans des
        environnements d’exploitation sécurisés et uniquement accessibles aux employés, agents
        et sous-traitants de Nestlé sur la base du principe de la connaissance sélective. Nestlé
        applique également les normes généralement acceptées à cet égard dans le secteur.</li>
      <li>Le cryptage des informations de paiement - Nestlé utilise un cryptage à la norme du
        secteur pour assurer la protection des informations financières sensibles, comme les
        informations de carte de crédit communiquées par le biais de l’Internet (ex. lorsque
        vous effectuez des paiements par le biais des magasins en ligne de Nestlé).</li>
      <li>L’authentification préalable pour accéder au compte - Nestlé demande à ses utilisateurs
        enregistrés de vérifier leur identité (ex. Identifiant et mot de passe d’ouverture de
        session) avant de les autoriser à accéder à leur compte ou d’y apporter des modifications.
        Ceci a pour objet d’empêcher les accès non autorisés.</li>
    </ul>
    <p>Veuillez noter que ces protections ne s’appliquent pas aux données personnelles que vous
      choisissez de partager dans les zones publiques comme les sites web communautaires.</p>
    <h3>b) Conservation</h3>
    <p>Nestlé ne conservera vos données personnelles qu’aussi longtemps qu’elles seront
      nécessaires aux fins déclarées, en tenant compte de notre besoin de pouvoir répondre
      aux demandes de renseignements ou de résoudre des problèmes, de fournir des services
      améliorés ou de nouveaux services et de respecter les obligations légales résultant des
      lois applicables. Ceci signifie que nous pouvons conserver vos données personnelles
      pendant une durée de temps raisonnable après votre dernière interaction avec nous.
      Lorsque les données personnelles que nous collectons ne sont plus nécessaires à ces
      fins, nous les détruisons ou les effaçons de manière sûre.</p>

    <h2 id="disclaimer8">8. Contactez-nous</h2>
    <p>Nestlé fait office de &quot;contrôleur de données&quot; pour les données personnelles qu’elle
      traite dans le cadre de cette Déclaration de Confidentialité.. Si vous souhaitez poser
      des questions ou effectuer des commentaires concernant la présente Déclaration de
      Confidentialité ou les pratiques de collecte des donnée personnelles de Nestlé, veuillez
      nous contacter par</p>
    <p>Téléphone au +32 (0)2 529 55 25<br />
      Courriel à consum@be.nestle.com<br />
      Courrier postal à Nestlé Belgilux SA, Service consommateurs, Rue de Birmingham,
      221, 1070 Bruxelles
    </p>
    <p>Copyright © mai 2015 Nestlé</p>

  </div>
);

export default DisclaimerFr;
