/**
 * Popup Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import DisclaimerFr from './DisclaimerFr';
import DisclaimerNl from './DisclaimerNl';
import './styles.css';

const Popup = props => (
  <div className="popup">
    <button
      type="button"
      className="popup-fade"
      onClick={props.togglePopup}
    />
    <div className="popup-body">
      <Scrollbars>
        <div className="popup-content">
          {props.langName === 'nl' ? <DisclaimerNl /> : <DisclaimerFr />}
        </div>
      </Scrollbars>
    </div>
  </div>
);

Popup.propTypes = {
  togglePopup: PropTypes.func.isRequired,
  langName: PropTypes.string,
};

Popup.defaultProps = {
  langName: undefined,
};

export default Popup;
