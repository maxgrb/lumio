/**
 * PageMerci component
 */
import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const PageMerci = props => (
  <div
    role="button"
    tabIndex={0}
    className="app-page page-merci"
    onClick={props.onRestart}
  >
    <div className="app-page-caption">
      {props.lang.wePreparePersonalizedBeverage}
    </div>
  </div>
);

PageMerci.propTypes = {
  lang: PropTypes.object.isRequired,
  onRestart: PropTypes.func.isRequired,
};

export default PageMerci;
