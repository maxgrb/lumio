/**
 * Merci Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

const Merci = props => (
  <button
    type="button"
    className="navigation-merci"
    onClick={props.onRestart}
  >
    {props.label}
  </button>
);

Merci.propTypes = {
  label: PropTypes.string.isRequired,
  onRestart: PropTypes.func.isRequired,
};

export default Merci;
