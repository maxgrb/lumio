/**
 * Export all components
 */
import ButtonRestart from './ButtonRestart';
import Checkbox from './Checkbox';
import Logo from './Logo';
import Navigation from './Navigation';
import NavigationLanguage from './NavigationLanguage';
import NavigationMerci from './NavigationMerci';
import PageDrinks from './PageDrinks';
import PageDrinksCapsule from './PageDrinksCapsule';
import PageMachines from './PageMachines';
import PageMerci from './PageMerci';
import PageMoments from './PageMoments';
import PagePersonal from './PagePersonal';
import PopupDisclaimer from './PopupDisclaimer';

export {
  ButtonRestart,
  Checkbox,
  Logo,
  Navigation,
  NavigationLanguage,
  NavigationMerci,
  PageDrinks,
  PageDrinksCapsule,
  PageMachines,
  PageMerci,
  PageMoments,
  PagePersonal,
  PopupDisclaimer,
};
