/**
 * Navigation Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ButtonRestart } from '../../components';
import './styles.css';

const StepName = ({ name }) => (
  <div className="navigation-name">
    {name}
  </div>
);

const Rounds = ({ step }) => (
  <div className="navigation-steps">
    <div className={classNames('navigation-step', { on: step === 1 })} />
    <div className={classNames('navigation-step', { on: step === 2 })} />
    <div className={classNames('navigation-step', { on: step === 3 })} />
    <div className={classNames('navigation-step', { on: step === 4 })} />
  </div>
);

const Navigation = props => (
  <div className="navigation">
    <div className={classNames('navigation-bar', { [`step-${props.step}`]: props.step })}>
      <StepName name={props.name} />
      {props.step > 1 &&
        <button
          type="button"
          className="navigation-button-back"
          onClick={props.onBack}
        />
      }
      <Rounds step={props.step} />
    </div>
    <ButtonRestart
      label={props.backLabel}
      onClick={props.onRestart}
    />
  </div>
);

Navigation.propTypes = {
  step: PropTypes.oneOf([1, 2, 3, 4]).isRequired,
  name: PropTypes.string.isRequired,
  backLabel: PropTypes.string.isRequired,
  onBack: PropTypes.func.isRequired,
  onRestart: PropTypes.func.isRequired,
};

export default Navigation;
