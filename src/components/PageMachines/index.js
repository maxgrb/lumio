/**
 * PageMachines component
 */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import './styles.css';

const PageMachine = (props) => {
  const selected = props.name === props.selected;

  const onSelect = () =>
    (selected ? props.nextStep() : props.selectItem(props.name));

  return (
    <button
      type="button"
      onClick={onSelect}
      className={classNames('page-machines-item', {
        red: props.index === 0,
        black: props.index === 1,
        white: props.index === 2,
        selected,
      })}
    >
      <div className="page-machines-item-title">
        {props.name}
        <div className="page-machines-item-confirm">
          {props.lang.confirm}
        </div>
      </div>
    </button>
  );
};

const PageMachines = (props) => {
  const renderMachine = (machine, index) => (
    <PageMachine
      key={machine}
      name={machine}
      index={index}
      {...props}
    />
  );

  return (
    <div className="app-page page-machines">
      <div className="app-page-caption">
        {props.lang.chooseYourLumio}
      </div>
      <div className="page-machines-items">
        {props.lang.machines.map(renderMachine)}
      </div>
    </div>
  );
};

PageMachines.propTypes = {
  lang: PropTypes.object.isRequired,
};

export default PageMachines;
