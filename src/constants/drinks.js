export default [
  {
    id: '90424014',
    nameFr: 'Espresso',
    nameNl: 'Espresso',
    capsules: [
      {
        id: 'cbee0965',
        name: 'Espresso',
        image: 'espresso.png',
      },
      {
        id: '33cbb329',
        name: 'Espresso Intenso',
        image: 'espresso-intenso.png',
      },
      {
        id: 'fbdd93e6',
        name: 'Barista',
        image: 'barista.png',
      },
      {
        id: '977fde45',
        name: 'Espresso Caramel',
        image: 'espresso-caramel.png',
      },
      {
        id: '501bed92',
        name: 'Ristretto Ardenza',
        image: 'ristretto-ardenza.png',
      },
      {
        id: '54c7519c',
        name: 'Essenza Di Moka',
        image: 'essenza-di-moka.png',
      },
      {
        id: 'eb6b6126',
        name: 'Cortado',
        image: 'cortado.png',
      },
    ],
  },
  {
    id: 'd20d2b3a',
    nameFr: 'Cappuccino et latte',
    nameNl: 'Cappuccino & Latte',
    capsules: [
      {
        id: '7e134f80',
        name: 'Cappuccino',
        image: 'cappuccino.png',
      },
      {
        id: 'e9455777',
        name: 'Cappuccino Soja',
        image: 'cappuccino-soja.png',
      },
      {
        id: 'b2231a44',
        name: 'Cappuccino Light',
        image: 'cappuccino-light.png',
      },
      {
        id: '5fc4fcfb',
        name: 'Café au Lait',
        image: 'cafe-au-lait.png',
      },
      {
        id: '4dba1007',
        name: 'Café au lait Décaféiné',
        image: 'cafe-au-lait-decafeine.png',
      },
      {
        id: '276fb5e2',
        name: 'Latte Macchiato',
        image: 'latte-macchiato.png',
      },
      {
        id: 'fbfed756',
        name: 'Latte Macchiato Caramel',
        image: 'latte-macchiato-caramel.png',
      },
      {
        id: '91a92fe9',
        name: 'Latte Macchiato Vanille',
        image: 'latte-macchiato-vanille.png',
      },
    ],
  },
  {
    id: '82cb3340',
    nameFr: 'Lungo',
    nameNl: 'Lungo',
    capsules: [
      {
        id: '5dcf2296',
        name: 'Americano',
        image: 'americano.png',
      },
      {
        id: '484277b9',
        name: 'Preludio',
        image: 'preludio.png',
      },
      {
        id: 'de4b23e1',
        name: 'Grande',
        image: 'grande.png',
      },
      {
        id: '0d23cfc3',
        name: 'Grande Intenso',
        image: 'grande-intenso.png',
      },
      {
        id: '2c009a37',
        name: 'Lungo',
        image: 'lungo.png',
      },
      {
        id: 'd807e6a2',
        name: 'Lungo Intenso',
        image: 'lungo-intenso.png',
      },
      {
        id: 'bb6ddf7e',
        name: 'Lungo Mild',
        image: 'lungo-mild.png',
      },
      {
        id: 'c82d2925',
        name: 'Lungo Decaffeinato',
        image: 'lungo-decaffeinato.png',
      },
    ],
  },
  {
    id: '55624626',
    nameFr: 'Chocolat chaud',
    nameNl: 'Warme chocolademelk',
    capsules: [
      {
        id: '613e7322',
        name: 'Chococino',
        image: 'chococino.png',
      },
      {
        id: '3a660f0b',
        name: 'Nesquik',
        image: 'nesquik.png',
      },
      {
        id: '6a1d3707',
        name: 'Choco Caramel',
        image: 'choco-caramel.png',
      },
      {
        id: '5dcf2296',
        name: 'Mocha',
        image: 'mocha.png',
      },
    ],
  },
  {
    id: 'd5439ef4',
    nameFr: 'Thé',
    nameNl: 'Thee',
    capsules: [
      {
        id: 'dbfd21a8',
        name: 'Citrus Honey Black Tea',
        image: 'citrus-honey-black-tea.png',
      },
      {
        id: 'f6b2951f',
        name: 'Marrakesh Tea',
        image: 'marrakesh-tea.png',
      },
      {
        id: '895c642f',
        name: 'Chai Tea Latte',
        image: 'chai-tea-latte.png',
      },
      {
        id: '04c49467',
        name: 'Tea Latte',
        image: 'tea-latte.png',
      },
    ],
  },
];
