/**
 * Constants
 */
import langFr from './langFr';
import langNl from './langNl';
import drinks from './drinks';

export const langs = {
  fr: langFr,
  nl: langNl,
};

export { drinks };

export const steps = {
  LANGUAGE: 'LANGUAGE',
  PERSONAL: 'PERSONAL',
  DRINKS: 'DRINKS',
  MACHINES: 'MACHINES',
  MOMENTS: 'MOMENTS',
  MERCI: 'MERCI',
};

export const pages = [
  steps.LANGUAGE,
  steps.PERSONAL,
  steps.DRINKS,
  steps.MACHINES,
  steps.MOMENTS,
  steps.MERCI,
];

export const types = {
  LANGUAGE_SELECT: 'LANGUAGE_SELECT',
  STEP_NEXT: 'STEP_NEXT',
  STEP_PREVIOUS: 'STEP_PREVIOUS',
  RESTART: 'RESTART',
  INPUT_CHANGE: 'INPUT_CHANGE',
  CHECKBOX_TOGGLE: 'CHECKBOX_TOGGLE',
  POPUP_DISCLAIMER_TOGGLE: 'POPUP_DISCLAIMER_TOGGLE',
  ITEM_SELECT: 'ITEM_SELECT',
  ITEM_DESELECT: 'ITEM_DESELECT',
  SCROLL_TOGGLE: 'SCROLL_TOGGLE',
  DATA_SUBMIT: 'DATA_SUBMIT',
};
