/**
 * Actions
 */
import request from 'superagent';
import { types } from '../constants';

export const nextStep = () => ({
  type: types.STEP_NEXT,
});

export const previousStep = () => ({
  type: types.STEP_PREVIOUS,
});

export const restart = () => ({
  type: types.RESTART,
});

export const selectLanguage = lang => (
  dispatch => (
    Promise.resolve()
      .then(() => dispatch({ type: types.LANGUAGE_SELECT, lang }))
      .then(() => dispatch(nextStep()))
  )
);

export const changeInput = (input, value) => ({
  type: types.INPUT_CHANGE,
  input,
  value,
});

export const toggleCheckbox = checkbox => ({
  type: types.CHECKBOX_TOGGLE,
  checkbox,
});

export const selectItem = (item, value) => ({
  type: types.ITEM_SELECT,
  item,
  value,
});

export const deselectItem = item => ({
  type: types.ITEM_DESELECT,
  item,
});

export const toggleScroll = () => ({
  type: types.SCROLL_TOGGLE,
});

export const togglePopupDisclaimer = () => ({
  type: types.POPUP_DISCLAIMER_TOGGLE,
});

export const submitData = () => (
  (dispatch, getState) => {
    const {
      inputName: name,
      inputEmail: email,
      checkboxSubscription: subscription,
      lang,
      drink,
      machine,
      moment,
    } = getState();

    return request
      .post('submit.php')
      .type('form')
      .send({ lang: lang.langName, name, email, subscription, drink, machine, moment })
      .end(() => dispatch({ type: types.DATA_SUBMIT }));
  }
);
