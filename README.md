# Lumio Dolce Gusto

## Built with

* [React.js](https://github.com/facebook/react) – front-end library for building UI.
* [Redux](https://github.com/reactjs/redux/) – state container for structuring business logic in a predictable way.
* [Webpack](https://github.com/webpack/webpack) – module bundler for concatenating and minifying JavaScript.

## Structure

`/public` – index.html and images which copy to `/build` when it builds.

`/src` – source files

* `actions` – Redux actions.
* `components`– React stateless components.
* `constants`– steps ids, langs, drinks data, redux action types.
* `containers` – the main app container.
* `reducers` – App state and reducer function.
* `shared` – fonts and styles.

## Install

1. Install [Node.js](https://nodejs.org/en/).
2. Install Node Package Manager `sudo npm i npm -g`.
3. Install all dependencies `npm i`.
4. Now you can run development server on localhost `npm start` or build the bundle `npm run build` for production.

## Develop

Run `npm start`. It runs the app in development mode on `localhost:8080` with hot loading. The app also is available by IP address of the host and port 8080 from devices, connected to the same local network. It's useful for testing on different devices.

## Build

Run `npm run build`. It installs dependencies, compiles JS-bundle and updates `/build`.
