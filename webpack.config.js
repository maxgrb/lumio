const { resolve } = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// PostCSSs plugins
const partialImport = require('postcss-partial-import');
const stylelint = require('stylelint');
const precss = require('precss');
const autoprefixer = require('autoprefixer');
const postcssCalc = require('postcss-calc');
const postcssReporter = require('postcss-reporter');

const isDev = process.env.NODE_ENV === 'development' || false;
const buildPath = resolve(__dirname, 'build');
const nodeModules = resolve(__dirname, 'node_modules');

// HTML templates for HtmlWebpackPlugin
const htmlTemplate = {
  template: 'public/index.html',
  filename: 'index.html',
};

module.exports = {
  devtool: isDev ? 'eval-source-map' : false,

  entry: [
    resolve(__dirname, 'index.js'),
  ],

  output: {
    path: buildPath,
    filename: 'js/bundle-[hash:6].js',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: nodeModules,
        loader: 'eslint-loader',
        enforce: 'pre',
      },
      {
        test: /\.js$/,
        exclude: nodeModules,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                partialImport,
                stylelint,
                precss,
                postcssCalc,
                autoprefixer,
                postcssReporter({ clearMessages: true }),
              ],
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        loader: 'svg-url-loader',
      },
      {
        test: /\.(jpe?g|png)$/i,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'images/[name].[ext]',
          },
        }],
      },
      {
        test: /\.woff|woff2$/,
        use: [{
          loader: 'url-loader',
        }],
      },
    ],
  },

  plugins: isDev ? [
    new HtmlWebpackPlugin(htmlTemplate),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(process.env.NODE_ENV) },
    }),
  ] : [
    new HtmlWebpackPlugin(htmlTemplate),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(process.env.NODE_ENV) },
    }),
  ],

  resolve: {
    extensions: ['.js'],
    modules: [nodeModules],
  },

  devServer: {
    hot: isDev,
    host: '0.0.0.0',
    port: 8080,
    contentBase: buildPath,
    historyApiFallback: true,
    inline: isDev,
    compress: !isDev,
  },
};
